﻿using ru.sazan.trader;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Data;
using ru.sazan.trader.Handlers;
using ru.sazan.trader.Models;
using ru.sazan.trader.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ru.sazan.trader.Utility;

namespace ru.sazan.scalper
{
    public class FollowTrendOnTick:AddedItemHandler<Tick>
    {
        private Strategy strategy;
        private OrderBookContext orderBook;
        private DataContext tradingData;
        private ObservableQueue<Signal> signalQueue;
        private int seconds;
        private double priceSpan;
        private Logger logger;

        public FollowTrendOnTick(Strategy strategy, 
            int seconds,
            double priceSpan,
            OrderBookContext orderBook, 
            DataContext tradingData, 
            ObservableQueue<Signal> signalQueue, 
            Logger logger)
            :base(tradingData.Get<ObservableCollection<Tick>>())
        {
            this.strategy = strategy;
            this.seconds = seconds;
            this.priceSpan = priceSpan;
            this.orderBook = orderBook;
            this.tradingData = tradingData;
            this.signalQueue = signalQueue;
            this.logger = logger;
        }

        public override void OnItemAdded(Tick item)
        {
            if (item.Symbol != this.strategy.Symbol)
                return;

            if (this.tradingData.GetAmount(this.strategy) != 0)
                return;

            IEnumerable<Tick> ticks = 
                this.tradingData.
                Get<IEnumerable<Tick>>().
                Last(item.Symbol, new TimeSpan(0, 0, this.seconds));

            if (ticks.Max(t => t.Price) - ticks.Min(t => t.Price) < this.priceSpan)
                return;

            double price = 0;
            Signal signal = null;

            if (ticks.Last().Price > ticks.First().Price)
            {
                price = this.orderBook.GetOfferPrice(this.strategy.Symbol, 0);
                signal = new Signal(this.strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Limit, price, 0, price);
            }
            else if (ticks.Last().Price < ticks.First().Price)
            {
                price = this.orderBook.GetBidPrice(this.strategy.Symbol, 0);
                signal = new Signal(this.strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Sell, OrderType.Limit, price, 0, price);
            }

            this.logger.Log(String.Format("Сгенерирован новый сигнал {0}", signal.ToString()));
            this.signalQueue.Enqueue(signal);

        }
    }
}
