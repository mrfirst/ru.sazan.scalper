﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;
using ru.sazan.trader.Collections;
using ru.sazan.trader;

namespace ru.sazan.scalper
{
    public class StopLossOnBar:MakeClosePositionSignalByPointsOnBar
    {
        public StopLossOnBar(Strategy strategy,
            double points,
            DataContext tradingData,
            ObservableQueue<Signal> signalQueue,
            Logger logger)
            : base(strategy,
            points,
            tradingData,
            signalQueue,
            logger) { }

        public override double CalculatePositionClosePrice()
        {
            Trade openTrade = this.tradingData.GetPositionOpenTrade(this.strategy);

            return openTrade.Amount > 0 ? openTrade.Price - this.points : openTrade.Price + this.points;
        }

        public override bool ItsTimeToClosePosition(trader.Models.Bar item, double closePrice, trader.Models.TradeAction closeAction)
        {
            if (closeAction == TradeAction.Buy && item.Close < closePrice)
                return false;

            if (closeAction == TradeAction.Sell && item.Close > closePrice)
                return false;

            return true;
        }
    }
}
